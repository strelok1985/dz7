//
//  ViewController.swift
//  dz7
//
//  Created by Andrey Bakanov on 3/25/19.
//  Copyright © 2019 Andrey Bakanov. All rights reserved.
//

import UIKit


class ViewController: UIViewController {
    
    var myDigitOne: Int = 0
    var myDigitTwo : Int = 0
    var mySumOfDigit : Int = 0
    let kRunCount = "runCount"
   
    @IBOutlet weak var textView: UITextView!
    //var textColor: UIColor
    
    @IBOutlet weak var textField2: UITextField!
    
    @IBOutlet weak var textField1: UITextField!
    
    @IBOutlet weak var labelCanter: UILabel!
    
    @IBAction func button(_ sender: UITextField) {
        
        myDigitOne = Int(textField1.text!)!
        myDigitTwo = Int(textField2.text!)!
        mySumOfDigit = myDigitOne + myDigitTwo
        labelCanter.font = labelCanter.font.withSize(CGFloat(mySumOfDigit))
        labelCanter.text = "\(mySumOfDigit)"
        
        let textHistoryResult: String = String(myDigitOne) + " + " + String(myDigitTwo) + " = " + String(mySumOfDigit)
        
        //let array: [String] = ["\(textHistoryResult)"]
        
//        var countDefaults = UserDefaults.standard.array(forKey : kRunCount)
//        if countDefaults == nil {countDefaults = array
//            textView.text  = "\(countDefaults!)"
//        UserDefaults.standard.set(countDefaults, forKey: kRunCount)
//        }
//        else if countDefaults != nil {UserDefaults.standard.set(countDefaults, forKey: kRunCount)
//        countDefaults?.append(array)
//        textView.text  = "\(countDefaults!)"
//            UserDefaults.standard.set(countDefaults, forKey: kRunCount)}

        
        var countDefaults = UserDefaults.standard.string(forKey : kRunCount)
        if countDefaults == nil{
            countDefaults = textHistoryResult + "\n"
            textView.text  = "\(countDefaults!)"
            UserDefaults.standard.set(countDefaults, forKey: kRunCount)
        }
        else if countDefaults != nil{
        countDefaults = countDefaults! + textHistoryResult +  "\n"
        textView.text  = "\(countDefaults!)"
        UserDefaults.standard.set(countDefaults, forKey: kRunCount)}

     
    }


}



